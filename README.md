[[_TOC_]]

# k8s-security

## breaking-k8s-series

### Session 1 - 19. #everyonecancontribute​ cafe

- [Code examples used in the Session](https://gitlab.com/everyonecancontribute/kubernetes/k8s-security/-/tree/main/session-1)
- [Bloqpost](https://everyonecancontribute.com/post/2021-03-03-cafe-19-break-into-kubernetes-security/)

## securing-k8s-series

### Session 2 - 20. #everyonecancontribute​ cafe

- [Code examples used in the Session](https://gitlab.com/everyonecancontribute/kubernetes/k8s-security/-/tree/main/session-2)
- [Bloqpost](https://everyonecancontribute.com/post/2021-03-10-cafe-20-securing-kubernetes-with-kyverno/)