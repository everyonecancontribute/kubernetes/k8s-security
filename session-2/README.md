# Session 2

## Preparations

use Namespaces, secrets & manifests from session-1

## Session

```bash
##################
### Showcase 1 ###
##################
kubectl -n kube-system describe pod API_POD
# Check if unsecure config params are present and clear them
- --anonymous-auth=true # Remove
- --insecure-port=8080 # set to --insecure-port=0
- --kubernetes-service-node-port=30001 # Remove
# restart API server and verify your changes
##################
### Showcase 2 ###
##################
# https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/
# create a secret
kubectl  -n everyonecancontribute create secret generic plaintext-secret --from-literal=password=PL4iNt3xT
# connect via shell session to the ETCD pod
kubectl -n kube-system exec -it ETCD_POD -- sh
# query ETCD to get the plain text secret we are searching for
ETCDCTL_API=3 etcdctl get /registry/secrets/everyonecancontribute/plaintext-secret --cacert=/etc/kubernetes/pki/etcd/ca.crt --cert=/etc/kubernetes/pki/etcd/server.crt --key=/etc/kubernetes/pki/etcd/server.key
# disconnect your session
# ssh into the first control plane node
# Generate a 32 byte random key and base64 encode it
head -c 32 /dev/urandom | base64
# # create a new folder in the kubernetes directory
mkdir /etc/kubernetes/api/
vim /etc/kubernetes/api/encryptconfig.yaml
# edit api server
vim /etc/kubernetes/manifests/kube-apiserver.yaml
# aded this line
--encryption-provider-config=/etc/kubernetes/api/encryptconfig.yaml
# restart api server with moving manifeste out of the directory
# disconnect your session
# create a new secret
kubectl -n everyonecancontribute create secret generic encrypted-secret --from-literal=password=EnCrYptEd
# connect to ETCD
# connect via shell session to the ETCD pod
kubectl -n kube-system exec -it ETCD_POD -- sh
# view secret encrypted
ETCDCTL_API=3 etcdctl get /registry/secrets/everyonecancontribute/encrypted-secret --cacert=/etc/kubernetes/pki/etcd/ca.crt --cert=/etc/kubernetes/pki/etcd/server.crt --key=/etc/kubernetes/pki/etcd/server.key
# view old secret unencrypted
ETCDCTL_API=3 etcdctl get /registry/secrets/everyonecancontribute/plaintext-secret --cacert=/etc/kubernetes/pki/etcd/ca.crt --cert=/etc/kubernetes/pki/etcd/server.crt --key=/etc/kubernetes/pki/etcd/server.key
# Ensure all secrets are encrypted
kubectl get secrets --all-namespaces -o json | kubectl replace -f -
##################
### Showcase 3 ###
##################
# install Kyverno
kubectl create -f https://raw.githubusercontent.com/kyverno/kyverno/main/definitions/release/install.yaml
# explore Kyverno
kubectl get crd | grep kyverno
kubectl -n kyverno get configmaps init-config -o yaml
kubectl get svc -n kyverno
kubectl get mutatingwebhookconfigurations -A
kubectl get validatingwebhookconfigurations -A | grep kyverno
kubectl -n kyverno get configmaps init-config -o yaml
# apply policies
kubectl apply -f ./policies/.
# get all Polcies
kubectl get clusterpolicies -A
# reort namespaces
kubectl get policyreport -A
# report not namespaced object
kubectl get clusterpolicyreport -A
# detailed view of the failed ones
kubectl describe clusterpolicyreport -A | grep -i "status: \+fail" -B10
kubectl describe cpolr -A | grep -i "status: \+pass" -B10
kubectl describe policyreport -n everyonecancontribute  | grep -i "status: \+fail" -B10
kubectl describe policyreport -n everyonecancontribute | grep -i "status: \+pass" -B10
kubectl describe polr -n philips-workspace  | grep -i "status: \+fail" -B10
kubectl describe polr -n philips-workspace  | grep -i "status: \+pass" -B10
# check images
kubectl get pods -A -o=custom-columns=NAMESPACE:.metadata.namespace,NAME:.metadata.name --sort-by="{.metadata.namespace}"
kubectl get pods -A -o=custom-columns=NAMESPACE:.metadata.namespace,NAME:.metadata.name,IMAGE:.spec.containers[*].image
# run a pod with the default serviceaccount mounted
kubectl run sa-test --image=nginx
# run a pod with privilged mode
kubectl apply -f https://raw.githubusercontent.com/BishopFox/badPods/main/manifests/priv/pod/priv-exec-pod.yaml
kubectl apply -f https://raw.githubusercontent.com/BishopFox/badPods/main/manifests/hostpid/pod/hostpid-exec-pod.yaml
# run an image from an unsecure registry
kubectl run unsecure-registry --image=quay.io/bitnami/nginx
```